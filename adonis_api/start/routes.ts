/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import 'App/Modules/User/routes'



// post route

Route.get('/posts', async (ctx) => {
  const { default: PostsController } = await import(
    'App/Controllers/Http/PostsController'
  )
  return new PostsController().index(ctx)
});

// user route

Route.get('/users', async (ctx) => {
  const { default: UsersController } = await import(
    'App/Controllers/Http/UsersController'
  )
  return new UsersController().index(ctx)
});


Route.get('/posts/create', () => {
  return 'Display a form to create a post'
})

Route.post('/posts', async () => {
  return 'Handle post creation form request'
})

Route.get('/posts/:id', () => {
  return 'Return a single post'
})

Route.get('/posts/:id/edit', () => {
  return 'Display a form to edit a post'
})

Route.put('/posts/:id', () => {
  return 'Handle post update form submission'
})

Route.delete('/posts/:id', () => {
  return 'Delete post'
})

Route.post('posts/all', async ({ request }) => {
  /**
   * Access the entire request body
   */
  console.log(request.body())

  /**
   * Access the parsed query string object
   */
  console.log(request.qs())

  /**
   * A merged copy of the request body and the query
   * string
   */
  console.log(request.all())

  /**
   * Cherry pick fields from the "request.all()" output
   */
  console.log(request.only(['title', 'description']))

  /**
   * Omit fields from the "request.all()" output
   */
  console.log(request.except(['csrf_token', 'submit']))

  /**
   * Access value for a single field
   */
  console.log(request.input('title'))
  console.log(request.input('description'))
})


